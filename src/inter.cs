using System;
using System.Collections.Generic;

namespace Fascal
{

    public class Program
    {

        public Data inMem;
        public Stack<Data> datas = new Stack<Data>();
        public Dictionary<string, Data> vars = new Dictionary<string, Data>();

        string code;

        public void Main()
        {
            string code = "";
            while (true)
            {
                string l = Console.ReadLine();
                if (l.Equals("exit")) { break; }
                code += l + "\n";
            }
            this.code = code;
            evaks(code);

        }

		public void Main(string code){
			this.code = code;
            evaks(code);
		}
        public void evaks(string code)
        {
            string bfr = "";
            bool instr = false;
            bool nClBfr = false;
            for (int i = 0; i < code.Length; i++)
            {
                char c = code[i];
                if (c == '"')
                {
                    instr = !instr;
                    if (bfr.Length == 0) { continue; }
                    runToken(bfr, TokenType.data, i, out i);
                    bfr = "";
                    continue;
                }
                if (instr)
                {
                    bfr += c;
                    continue;
                }
                if (c == ' ' || c == '\n' || c == '\t')
                {
                    if (bfr != "")
                    {
                        if (char.IsNumber(bfr[0]))
                        {
                            runToken(bfr, TokenType.data, i, out i);
                        }
                        else
                        {
                            if (bfr[0] == '@')
                            {
                                string lpd = bfr.Remove(0, 1);
                                runToken(lpd, TokenType.varn, i, out i);
                            }
                            else if (bfr[0] == '?')
                            {
                                string lpd = bfr.Remove(0, 1);
                                runStackOper(StackOKword.jumper, i, out i, lpd);
                                if (i == 0)
                                {
                                    bfr = "" + code[0];
                                    nClBfr = true;
                                }
                            }
                            else
                            {
                                runToken(bfr, TokenType.oper, i, out i);
                            }
                        }
                        if (nClBfr)
                        {
                            nClBfr = false;
                        }
                        else
                        {
                            bfr = "";
                        }

                    }
                    continue;
                }
                bfr += c;
            }
            if (bfr != "")
            {
                int idn = 0;
                if (char.IsNumber(bfr[0]))
                {
                    runToken(bfr, TokenType.data, 0, out idn);
                }
                else
                {
                    if (bfr[0] == '@')
                    {
                        string lpd = bfr.Remove(0, 1);
                        runToken(lpd, TokenType.varn, 0, out idn);
                    }
                    else if (bfr[0] == '?')
                    {
                        string lpd = bfr.Remove(0, 1);
                        runStackOper(StackOKword.jumper, code.Length, out idn, lpd);
                    }
                    else
                    {
                        runToken(bfr, TokenType.oper, 0, out idn);
                    }
                }
            }
        }

        public void runToken(string code, TokenType tt, int chLl, out int chL)
        {
            if (tt == TokenType.data)
            {
                chL = chLl;
                if (code.Length == 0) { return; }
                if (char.IsNumber(code[0]))
                {
                    datas.Push(new Data(DataType.iint, code));
                }
                else
                {
                    datas.Push(new Data(DataType.sstring, code));
                }
            }
            else if (tt == TokenType.oper)
            {
                if (code == " " || code == "" || code == "  ")
                {
                    chL = chLl;
                    return;
                }
                if (keywords.ContainsKey(code))
                {
                    StackOKword k = keywords[code];
                    int o = 0;
                    runStackOper(k, chLl, out o, code);
                    chL = o;
                    return;
                }
                if (consts.ContainsKey(code))
                {
                    datas.Push(new Data(DataType.bbool, code));
                    chL = chLl;
                    return;
                }
                Console.WriteLine("unknown operation token: " + code);
                Errors.Errors.exit();
            }
            else if (tt == TokenType.varn)
            {
                chL = chLl;
                if (code.Length == 0) { return; }
                datas.Push(new Data(DataType.varn, code));
            }
            else
            {
                Console.WriteLine("wtf");
            }
            chL = chLl;
        }

        public void runStackOper(StackOKword type, int c1, out int c2, string cmd)
        {
            switch (type)
            {
                case StackOKword.plus:
                    c2 = c1;
                    throwEmptyStackErrorIfTrue();
                    Data d1 = datas.Pop();
                    throwEmptyStackErrorIfTrue();
                    Data d2 = datas.Pop();
                    datas.Push(
                        new Data(
                                DataType.iint,
                                (int.Parse(d1.val) + int.Parse(d2.val)).ToString()
                            )
                    );
                    break;
                case StackOKword.puts:
                    c2 = c1;
                    throwEmptyStackErrorIfTrue();
                    string sp1 = datas.Pop().val;
                    string pp = "";
                    pp = sp1.Replace("\\n", "\n");
                    Console.Write(pp);
                    break;
                case StackOKword.dup:
                    c2 = c1;
                    throwEmptyStackErrorIfTrue();
                    datas.Push(datas.Peek());
                    break;
                case StackOKword.minus:
                    c2 = c1;
                    throwEmptyStackErrorIfTrue();
                    Data d11 = datas.Pop();
                    throwEmptyStackErrorIfTrue();
                    Data d22 = datas.Pop();
                    datas.Push(
                        new Data(
                                DataType.iint,
                                (int.Parse(d22.val) - int.Parse(d11.val)).ToString()
                            )
                    );
                    break;
				case StackOKword.times:
					c2 = c1;
                    throwEmptyStackErrorIfTrue();
                    Data d1112 = datas.Pop();
                    throwEmptyStackErrorIfTrue();
                    Data d2222 = datas.Pop();
                    datas.Push(
                        new Data(
                                DataType.iint,
                                (int.Parse(d2222.val) * int.Parse(d1112.val)).ToString()
                            )
                    );
                    break;
				case StackOKword.divide:
					c2 = c1;
                    throwEmptyStackErrorIfTrue();
                    Data d1111 = datas.Pop();
                    throwEmptyStackErrorIfTrue();
                    Data d22222 = datas.Pop();
                    datas.Push(
                        new Data(
                                DataType.iint,
                                (Math.Round((decimal)int.Parse(d22222.val) / int.Parse(d1111.val))).ToString()
                            )
                    );
                    break;
                case StackOKword.drop:
                    c2 = c1;
                    throwEmptyStackErrorIfTrue();
                    datas.Pop();
                    break;
                case StackOKword.swap:
                    c2 = c1;
                    throwEmptyStackErrorIfTrue();
                    Data d111 = datas.Pop();
                    throwEmptyStackErrorIfTrue();
                    Data d222 = datas.Pop();
                    datas.Push(d111);
                    datas.Push(d222);
                    break;
                case StackOKword.equal:
                    c2 = c1;
                    throwEmptyStackErrorIfTrue();
                    Data d12 = datas.Pop();
                    throwEmptyStackErrorIfTrue();
                    Data d21 = datas.Pop();
                    if (d12.dt == d21.dt)
                    {
                        if (d12.val == d21.val)
                        {
                            datas.Push(new Data(DataType.bbool, "true"));
                            break;
                        }
                    }
                    datas.Push(new Data(DataType.bbool, "false"));
                    break;
                case StackOKword.less:
                    c2 = c1;
                    throwEmptyStackErrorIfTrue();
                    Data d13 = datas.Pop();
                    throwEmptyStackErrorIfTrue();
                    Data d23 = datas.Pop();
                    if (d13.dt == d23.dt)
                    {

                        if (d13.dt == DataType.iint)
                        {
                            if (int.Parse(d13.val) > int.Parse(d23.val))
                            {
                                datas.Push(new Data(DataType.bbool, "true"));
                                break;
                            }
                        }
                    }
                    datas.Push(new Data(DataType.bbool, "false"));
                    break;
                case StackOKword.more:
                    c2 = c1;
                    throwEmptyStackErrorIfTrue();
                    Data d14 = datas.Pop();
                    throwEmptyStackErrorIfTrue();
                    Data d24 = datas.Pop();
                    if (d14.dt == d24.dt)
                    {
                        if (d14.dt == DataType.iint)
                        {
                            if (int.Parse(d14.val) < int.Parse(d24.val))
                            {
                                datas.Push(new Data(DataType.bbool, "true"));
                                break;
                            }
                        }
                    }
                    datas.Push(new Data(DataType.bbool, "false"));
                    break;
                case StackOKword.nequal:
                    c2 = c1;
                    throwEmptyStackErrorIfTrue();
                    Data d15 = datas.Pop();
                    throwEmptyStackErrorIfTrue();
                    Data d25 = datas.Pop();
                    if (d15.dt == d25.dt)
                    {
                        if (d15.val != d25.val)
                        {
                            datas.Push(new Data(DataType.bbool, "true"));
                            break;
                        }
                    }
                    datas.Push(new Data(DataType.bbool, "false"));
                    break;
                case StackOKword.neg:
                    c2 = c1;
                    throwEmptyStackErrorIfTrue();
                    Data d17 = datas.Pop();
                    if (d17.dt == DataType.bbool)
                    {
                        if (d17.val == "true")
                        {
                            datas.Push(new Data(DataType.bbool, "false"));
                        }
                        else
                        {
                            datas.Push(new Data(DataType.bbool, "true"));
                        }
                    }
                    else
                    {
                        Console.WriteLine("error");
                    }
                    break;
                case StackOKword.exit:
                    c2 = c1;
                    Errors.Errors.exit();
                    break;
                case StackOKword.getv:
                    c2 = c1;
                    throwEmptyStackErrorIfTrue();
                    Data d18 = datas.Pop();
                    if (d18.dt != DataType.varn) { Errors.Errors.throwError("required var name, but outher data was on top of stack"); }

                    string vrn = d18.val;
                    if (!vars.ContainsKey(vrn)) { Errors.Errors.throwError("var named " + vrn + "is is not declared in memory"); }
                    datas.Push(new Data(vars[vrn].dt, vars[vrn].val));
                    break;
                case StackOKword.setv:
                    c2 = c1;
                    throwEmptyStackErrorIfTrue();
                    Data d19 = datas.Pop();
                    if (d19.dt != DataType.varn) { Errors.Errors.throwError("required var name, but outher data was on top of stack"); }
                    throwEmptyStackErrorIfTrue();
                    Data d29 = datas.Pop();
                    string vrnn = d19.val;
                    if (!vars.ContainsKey(vrnn))
                    {
                        vars.Add(vrnn, new Data(d29.dt, d29.val));
                        break;
                    }
                    vars[vrnn] = new Data(d29.dt, d29.val);
                    break;
                case StackOKword.iif:
                    throwEmptyStackErrorIfTrue();
                    Data d191 = datas.Pop();
                    if (d191.val == "true" && d191.dt == DataType.bbool)
                    {
                        c2 = c1;
                        break;
                    }
                    else
                    {
                        int depth = 1;
                        bool inStr = false;
                        while (c1 < code.Length - 1)
                        {
                            c1 += 1;
                            if (code[c1] == '\"')
                            {
                                inStr = !inStr;
                                continue;
                            }
                            if (depth == 0)
                            {
                                break;
                            }
                            if (code[c1] == 'd'
                            && code[c1 - 1] == 'n'
                            && code[c1 - 2] == 'e')
                            {
                                depth -= 1;
                            }
                            else if (code[c1] == 'f'
                          && code[c1 - 1] == 'i')
                            {
                                depth += 1;
                            }
                        }
                        if (depth != 0)
                        {
                            Errors.Errors.throwError("inconsistent number of \"if\" and \"end\" tokens");
                        }
                    }
                    c2 = c1;
                    break;
                case StackOKword.end:
                    c2 = c1;
                    break;
                case StackOKword.jumper:
                    if (cmd.Length != 0)
                    {
                        if (cmd.Length == 1)
                        {
                            if (!char.IsDigit(cmd[0])) { Errors.Errors.throwError("expected number in jump definition"); }
                        }
                        else
                        {
                            if (!char.IsDigit(cmd[1])) { Errors.Errors.throwError("expected number in jump definition"); }
                        }
                    }

                    if (cmd.Contains(".")) { Errors.Errors.throwError("expected non-deciaml number"); }
                    int p = int.Parse(cmd);

                    if (p < 0)
                    {
                        p -= 1;
                        while (c1 != 0)
                        {
                            c1 -= 1;
                            if (code[c1] == '\n')
                            {
                                p += 1;
                                if (p == 0)
                                {
                                    c2 = c1 - 1;
                                    break;
                                }
                            }
                        }
                        c2 = c1;
                    }
                    else if (p > 0)
                    {
                        while (c1 < code.Length - 1)
                        {
                            c1 += 1;
                            if (code[c1] == '\n')
                            {
                                p -= 1;
                                if (p == 0)
                                {
                                    c2 = c1;
                                    break;
                                }
                            }
                        }
                        c2 = c1;
                    }
                    else
                    {
                        Errors.Errors.throwError("expected non-zero value");
                        c2 = c1;
                    }

                    break;
                default:
                    c2 = c1;
                    Console.WriteLine("unimplemented stack operation");
                    break;
            }
        }

        private void throwEmptyStackErrorIfTrue()
        {
            if (datas.ToArray().Length == 0) { Errors.Errors.throwEmptyStackError(); }
        }

        public enum StackOKword
        {
            drop,
            swap,
            dup,
            plus,
            minus,
			times,
			divide,
            puts,
            less,
            more,
            equal,
            nequal,
            neg,
            exit,
            setv,
            getv,
            iif,
            end,
            jumper,
        }

        public Dictionary<string, StackOKword> keywords = new Dictionary<string, StackOKword>(){
            {"drop",StackOKword.drop},
            {"swap",StackOKword.swap},
            {"dup",StackOKword.dup},
            {"+",StackOKword.plus},
            {"plus",StackOKword.plus},
            {"-",StackOKword.minus},
            {"minus",StackOKword.minus},
			{"*",StackOKword.times},
			{"/",StackOKword.divide},
            {"puts",StackOKword.puts},
            {"<",StackOKword.less},
            {">",StackOKword.more},
            {"=",StackOKword.equal},
            {"!=",StackOKword.nequal},
            {"neg",StackOKword.neg},
            {"!",StackOKword.neg},
            {"exit",StackOKword.exit},
            {"setv",StackOKword.setv},
            {"getv",StackOKword.getv},
            {"if",StackOKword.iif},
            {"end",StackOKword.end},
        };


        public Dictionary<string, DataType> consts = new Dictionary<string, DataType>(){
            {"true",DataType.bbool},
            {"false",DataType.bbool},
        };

        public enum DataType
        {
            iint,
            sstring,
            bbool,
            varn,
        }

        public enum TokenType
        {
            oper,
            data,
            cconst,
            varn,
        }

        public struct Token
        {
            public Data? dtt;
            public TokenType tokt;
            public StackOKword? opk;

            public Token(TokenType tokenType, StackOKword s)
            {
                this.tokt = tokenType;
                this.dtt = null;
                this.opk = s;
            }

            public Token(TokenType tokenType, Data dt)
            {
                this.tokt = tokenType;
                this.dtt = dt;
                this.opk = null;
            }

        }

        public struct Data
        {
            public DataType dt;
            public string val;
            public Data(DataType dt, string val)
            {
                this.dt = dt;
                this.val = val;
            }
        }
    }
}
