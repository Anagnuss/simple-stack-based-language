using Fascal;
using System;

public static class main{

    public static Program prg;
    public static void Main(string[] args){
        prg = new Program();

        if(args.Length==0){
            prg.Main();
        } else if(args.Length==1){
            if(!System.IO.File.Exists(args[0])){
                Errors.Errors.throwError("file not found!");
            }
            prg.Main(String.Join("\n",System.IO.File.ReadAllLines(args[0])));
        }
        
        
        Console.Write("\nPress any key to exit . . .");
        Console.ReadKey();
    }
}