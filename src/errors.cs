using System;

namespace Errors{
    public class Errors{

        public static void throwEmptyStackError(){
            throwError("stack is empty but code try to get value");
        }
        public static void throwError(string message){
            System.Console.WriteLine("ERROR: " + message);
            exit(1);
        }

        public static void exit(){
            exit(0);
        }

        public static void exit(int code){
            Console.Write("\nPress any key to exit . . .");
            Console.ReadKey();
            System.Environment.Exit(code);
        }
    }
}
